const fetch = require('node-fetch');
const encodeUrl = require('encodeurl');
const optMap = require('./options_map').options;

const merchiumHeaders = {
  Authorization:
    'Basic b3JkZXJAZXJhLWdyb3VwLm9yZzpvdTNxazY0aWxFYjBqOW1Pdjc0MTd1T1Y3TzM4OVlaNQ==',
  'Content-Type': 'application/json;charset=utf-8'
};

const baseUrl = 'https://aziks.mymerchium.ru/api';

const rustApi = 'http://rustrus.ru/api/';
const rustUser = 'newtest@test.ru';
const rustPass = 'password';

let rustProducts = Array();
let merchCategories = Array();

async function getAllRustProducts() {
  //TODO: remove brand filter
  await fetch(
    `${rustApi}/products.json?email=${rustUser}&password=${rustPass}&brand_id=247`
  )
    .then(response => response.json())
    .then(result => {
      for (let product of result) {
        rustProducts.push(product);
      }
    });
}

async function getMerchiumProduct(name) {
  return await fetch(`${baseUrl}/products?pname=Y&q=${encodeUrl(name)}`, {
    method: 'GET',
    headers: merchiumHeaders
  })
    .then(response => response.json())
    .then(response => {
      return new Promise(function(resolve) {
        setTimeout(() => {
          resolve(response);
        }, 500);
      });
    })
    .catch(error => console.log(error));
}

async function getMerchiumCategoriesNum() {
  return await fetch(`${baseUrl}/categories`, {
    method: 'GET',
    headers: merchiumHeaders
  })
    .then(response => response.json())
    .then(response => response.params.total_items)
    .then(response => {
      return new Promise(function(resolve) {
        setTimeout(() => {
          resolve(response);
        }, 500);
      });
    })
    .catch(error => console.log(error));
}

async function getAllMerchiumCategories(num) {
  return await fetch(`${baseUrl}/categories?items_per_page=${num}`, {
    method: 'GET',
    headers: merchiumHeaders
  })
    .then(response => response.json())
    .then(response => response)
    .then(response => {
      return new Promise(function(resolve) {
        setTimeout(() => {
          resolve(response);
        }, 500);
      });
    })
    .catch(error => console.log(error));
}

function getMerchiumCategoriesIds(rustCats) {
  let result = Array();
  for (cat of rustCats) {
    let category = merchCategories.filter(x => {
      return x.category == cat.name && x.seo_name.indexOf('rustrus') != -1;
    })[0];
    result.push(category.category_id);
  }

  return result;
}

async function addProductToMerchium(product, productId, isUpdate) {
  let postBody = {};
  let categoriesIds = getMerchiumCategoriesIds(product.linen_kinds);
  postBody.product = product.name;
  postBody.category_ids = categoriesIds;
  postBody.main_category = categoriesIds[0];
  postBody.status = 'D';
  postBody.price = product.price;
  postBody.full_description = product.description;
  postBody.product_code = `rustrus_${product.id}`;
  postBody.tracking = 'O';

  postBody.main_pair = {
    detailed: {
      http_image_path: `http://rustrus.ru${product.images[0].full}`,
      image_path: `http://rustrus.ru${product.images[0].full}`
    }
  };

  postBody.image_pairs = Array();
  for (image of product.images) {
    postBody.image_pairs.push({
      detailed: {
        http_image_path: `http://rustrus.ru${image.full}`,
        image_path: `http://rustrus.ru${image.full}`
      }
    });
  }

  return await fetch(
    isUpdate ? `${baseUrl}/products/${productId}` : `${baseUrl}/products`,
    {
      method: isUpdate ? 'PUT' : 'POST',
      headers: merchiumHeaders,
      body: JSON.stringify(postBody)
    }
  )
    .then(response => response.json())
    .then(response => {
      return response.product_id;
    })
    .catch(error => {
      console.log(error);
    });
}

async function getOptionsWithVariants(newProductId) {
  return await fetch(`${baseUrl}/options?product_id=${newProductId}`, {
    method: 'GET',
    headers: merchiumHeaders
  })
    .then(response => response.json())
    .then(response => {
      return response;
    })
    .then(response => {
      return new Promise(function(resolve) {
        setTimeout(() => {
          resolve(response);
        }, 500);
      });
    })
    .catch(error => {
      console.log(error);
    });
}

async function addOptions(variants, productId) {
  let options = new Set();

  for (let i = 0; i < variants.length; i++) {
    for (option in variants[i]) {
      if (optMap[option]) {
        options.add(option);
      }
    }
  }

  for (let option of options) {
    let setOptions = new Set();

    for (let i = 0; i < variants.length; i++) {
      setOptions.add(variants[i][option]);
    }

    let arrVariants = Array();
    for (let option of setOptions) {
      arrVariants.push({ variant_name: option });
    }

    let jsonBody = {
      product_id: productId,
      option_name: optMap[option],
      inventory: 'Y',
      variants: arrVariants
    };

    await fetch(`${baseUrl}/2.0/options`, {
      method: 'POST',
      headers: merchiumHeaders,
      body: JSON.stringify(jsonBody)
    })
      .then(response => response.json())
      .then(response => {
        return new Promise(function(resolve) {
          setTimeout(() => {
            resolve(response);
          }, 500);
        });
      })
      .catch(error => {
        console.log(error);
      });
  }
}

async function addCombinationOptions(productId, opts, quantity) {
  await fetch(`${baseUrl}/2.0/combinations`, {
    method: 'POST',
    headers: merchiumHeaders,
    body: JSON.stringify({
      product_id: productId,
      combination: opts,
      amount: quantity
    })
  })
    .then(response => response.json())
    .then(response => {
      return new Promise(function(resolve) {
        setTimeout(() => {
          resolve(response);
        }, 500);
      });
    })
    .catch(error => {
      console.log(error);
    });
}

async function deleteOptionsFromGood(productId) {
  let optionsId = await fetch(`${baseUrl}/options?product_id=${productId}`, {
    method: 'GET',
    headers: merchiumHeaders
  })
    .then(response => response.json())
    .then(response => {
      return new Promise(function(resolve) {
        setTimeout(() => {
          resolve(Object.keys(response));
        }, 500);
      });
    })
    .catch(error => {
      console.log(error);
    });

  for (let optionId of optionsId) {
    await fetch(`${baseUrl}/2.0/options/${optionId}`, {
      method: 'DELETE',
      headers: merchiumHeaders,
      body: JSON.stringify({
        product_id: productId
      })
    })
      .then(response => {
        return new Promise(function(resolve) {
          setTimeout(() => {
            resolve(response);
          }, 500);
        });
      })
      .catch(error => {
        console.log(error);
      });
  }
}

async function run() {
  // Получаем список категорий с мерчиума
  let merchCatNum = await getMerchiumCategoriesNum();
  let response = await getAllMerchiumCategories(merchCatNum);
  for (cat of response.categories) {
    merchCategories.push(cat);
  }

  // Полчаем список продуктов с раста
  await getAllRustProducts();

  console.log(rustProducts.length);

  for (rustProduct of rustProducts) {
    let existingProducts = await getMerchiumProduct(rustProduct.name);

    if (
      existingProducts.products[0] &&
      existingProducts.products[0].product_code.indexOf(
        `rustus_${rustProduct.id}` != -1
      )
    ) {
      await addProductToMerchium(
        rustProduct,
        existingProducts.products[0].product_id,
        true
      );

      await deleteOptionsFromGood(existingProducts.products[0].product_id);

      await addOptions(
        rustProduct.variants,
        existingProducts.products[0].product_id
      );
      let optionsMap = await getOptionsWithVariants(
        existingProducts.products[0].product_id
      );
      console.log(rustProduct.variants);

      for (let i = 0; i < rustProduct.variants.length; i++) {
        let opts = {};
        for (let prop in optMap) {
          if (!rustProduct.variants[i][prop]) continue;

          let optId =
            optionsMap[
              Object.keys(optionsMap).find(
                key => optionsMap[key].option_name == optMap[prop]
              )
            ].option_id;

          let variantId =
            optionsMap[optId].variants[
              Object.keys(optionsMap[optId].variants).find(
                key =>
                  optionsMap[optId].variants[key].variant_name ==
                  rustProduct.variants[i][prop]
              )
            ].variant_id;

          opts[optId] = variantId;
        }

        await addCombinationOptions(
          existingProducts.products[0].product_id,
          opts,
          rustProduct.variants[i].quantity
        );
      }
    } else {
      let newProductId = await addProductToMerchium(rustProduct, 0, false);
      console.log(newProductId);

      await addOptions(rustProduct.variants, newProductId);
      let optionsMap = await getOptionsWithVariants(newProductId);
      console.log(rustProduct.variants);

      for (let i = 0; i < rustProduct.variants.length; i++) {
        let opts = {};
        for (let prop in optMap) {
          if (!rustProduct.variants[i][prop]) continue;

          let optId =
            optionsMap[
              Object.keys(optionsMap).find(
                key => optionsMap[key].option_name == optMap[prop]
              )
            ].option_id;

          let variantId =
            optionsMap[optId].variants[
              Object.keys(optionsMap[optId].variants).find(
                key =>
                  optionsMap[optId].variants[key].variant_name ==
                  rustProduct.variants[i][prop]
              )
            ].variant_id;

          opts[optId] = variantId;
        }

        await addCombinationOptions(
          newProductId,
          opts,
          rustProduct.variants[i].quantity
        );
      }
    }
  }
}

run();
