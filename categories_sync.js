const fetch = require('node-fetch');

const merchiumHeaders = {
  Authorization:
    'Basic b3JkZXJAZXJhLWdyb3VwLm9yZzpvdTNxazY0aWxFYjBqOW1Pdjc0MTd1T1Y3TzM4OVlaNQ==',
  'Content-Type': 'application/json;charset=utf-8'
};

const baseUrl = 'https://aziks.mymerchium.ru/api';

const rustApi = 'http://rustrus.ru/api/';
const rustUser = 'newtest@test.ru';
const rustPass = 'password';

let categories = new Set();
let rustCategories = new Set();

async function addCategoriesToMerchium() {
  for (rustCategory of rustCategories) {
    let arrCategories = Array.from(categories);
    existingCategory = arrCategories.filter(function(category) {
      return category.category == rustCategory && category.isRustus;
    });
    if (existingCategory.length == 0) {
      let strBody = JSON.stringify({
        category: rustCategory,
        status: 'D',
        seo_name: `rustrus ${rustCategory}`
      });

      let newCategory = await fetch(`${baseUrl}/categories`, {
        method: 'POST',
        headers: merchiumHeaders,
        body: strBody
      })
        .then(response => response.json())
        .then(response => {
          return new Promise(function(resolve, reject) {
            setTimeout(() => {
              resolve(response);
            }, 500);
          });
        })
        .then(result => {
          return {
            category_id: result.category_id.toString(),
            category: rustCategory
          };
        });
      categories.add(newCategory);
    }
  }
  console.log(categories);
}

async function getAllRustCategories() {
  await fetch(`${rustApi}/products.json?email=${rustUser}&password=${rustPass}`)
    .then(response => response.json())
    .then(result => {
      for (let product of result) {
        for (let category of product.linen_kinds) {
          rustCategories.add(category.name);
        }
      }
    });
  return rustCategories;
}

async function getAllCategories(count) {
  await fetch(`${baseUrl}/categories?items_per_page=${count}`, {
    headers: merchiumHeaders
  })
    .then(response => response.json())
    .then(result => {
      for (let category of result.categories) {
        categories.add({
          category_id: category.category_id,
          category: category.category,
          isRustus:
            category.seo_name && category.seo_name.indexOf('rustrus') != -1
        });
      }
    });
  return categories;
}

async function getCategoriesCount() {
  return await fetch(baseUrl + '/categories', { headers: merchiumHeaders })
    .then(response => response.json())
    .then(response => {
      return new Promise(function(resolve, reject) {
        setTimeout(() => {
          resolve(response);
        }, 1000);
      });
    })
    .then(response => {
      return response.params.total_items;
    });
}

async function sync() {
  let count = await getCategoriesCount();
  categories = await getAllCategories(count);
  rustCategories = await getAllRustCategories();
  await addCategoriesToMerchium();
}

sync();
